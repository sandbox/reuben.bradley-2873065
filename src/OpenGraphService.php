<?php

namespace Drupal\opengraph_utility;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use GuzzleHttp\ClientInterface;

/**
 * Open Graph Service class
 *
 */
class OpenGraphService {
	
    /**
     * An HTTP client
     * 
     * @var GuzzleHttp\ClientInterface
     */
    protected $httpClient;
    
    /**
     * Create an instance of the Open Graph Service, allowing for injection 
     *  of an HTTP client for use by the instance.
     * 
     * @param \GuzzleHttp\ClientInterface $http_client
     */
    public function __construct(ClientInterface $http_client) {
        $this->httpClient = $http_client;
    }
    
    /**
     * Get the meta tag values from the given URI. Optionally, specify the 
     *  fields that are desired. Fields specified that have no matching value
     *  at the URI will be skipped, possibly resulting in the return of an 
     *  empty array. Likewise if the URI given has no meta tags at all.
     * 
     * @param string $uri
     * @param array $fields
     * @return array
     */
	public function getOpenGraphInfo($uri, $fields = array()) {
        $tags = $this->getOpenGraphTags($uri);
        
        // Check if specific fields are being requested
        if ( !empty($fields) ) {
            $filteredTags = array();
            $fieldCount = count($fields);
            
            for ( $i = 0; $i < $fieldCount; $i++) {
                $key = $fields[$i];
                // Check for og (OpenGraph) and twitter prefixes
                if ( array_key_exists('og:' . $key, $tags) ) {
                    $filteredTags[$key] = $tags['og:' . $key];
                }
                elseif ( array_key_exists('twitter:' . $key, $tags) ) {
                    $filteredTags[$key] = $tags['twitter:' . $key];
                }
                elseif ( array_key_exists($key, $tags) ) {
                    $filteredTags[$key] = $tags[$key];
                }
                else {
                    continue;
                }
            }
            
            // Return the resulting array
            return $filteredTags;
        }
        
        // Return all available tags
        return $tags;
	}
	
    /**
     * Internal function used to retrieve all available meta tags at the given
     *  URI. Caches the page HTML and the resulting tags, if possible.
     * 
     * @param string $uri
     * @return array
     */
	protected function getOpenGraphTags($uri) {
        $cacheKey = md5($uri) . ':tags';
        $cacheBin = \Drupal::cache('opengraph_utility');
        
		// First, check the cache
        if ( $cache = $cacheBin->get($cacheKey) ) {
            return $cache->data;
        }
		
		// Nothing in cache? Time to scour the html
		if ( $html = $this->pullHtml($uri) ) {
			$tags = array('uri' => $uri);
			$found = array();
			
			// Pull the page title, if available
			if ( preg_match('/<title>(.*)<\/title>/Ui', $html, $found) ) {
				$tags['title'] = Html::decodeEntities($found[1]);
			}
			
			// Search for Open Graph meta tags
			$needle = '/<meta[^>]+(name|property)\s?=\s?([\'"]+)(.*)\2[^>]+(value|content)\s?=\s?([\'"]+)(.*)\5.*>/Ui';
			if ( preg_match_all($needle, $html, $found, PREG_PATTERN_ORDER) ) {
				// $found[3][x] will contain the name of the meta tag
				// $found[6][x] will contain the value for that tag
				$matchCount = count($found[0]);
				
				for ( $i = 0; $i < $matchCount; $i++ ) {
					// Remove any non-valid characters from the meta tag name
					$key = preg_replace('/[^a-z0-9:\._-]/', '', strtolower(trim($found[3][$i])));
					$value = Xss::filter(Html::decodeEntities(trim($found[6][$i])));
					
					// Ignore empty keys/values
					if ( !Unicode::strlen($key) || !Unicode::strlen($value) ) {
						continue;
					}
					
					$tags[$key] = $value;
				}
			}
		}
		
		// Write the tags to cache
        $cacheBin->set($cacheKey, $tags, (REQUEST_TIME + 3600));
		
		// Return the results
		return $tags;
	}
	
    /**
     * Internal function used to retrieve the HTML of the specified URI.
     *  Returns false if URI cannot be retrieved.
     * 
     * @param string $uri
     * @return string
     */
	protected function pullHtml($uri) {
		$cacheKey = md5($uri) . ':html';
		$cacheBin = \Drupal::cache('opengraph_utility');
		
		// First, look for the html in cache
		if ( $cache = $cacheBin->get($cacheKey) ) {
			return $cache->data;
		}
		
		// Looks like we need to make a request for the actual page!
		$request = $this->httpClient->get($uri, ['headers' => ['User-Agent' => 'OpenGraph Service (in testing) (+http://drupal.org/)']]);
		
		if ( $request->getStatusCode() == 200 ) {
			// Retrieve and cache the html
			$html = $request->getBody()->getContents();
			$cacheBin->set($cacheKey, $html, (REQUEST_TIME + 3600));
			
			// Return the resulting html
			return $html;
		}
		
		// Invalid response, return false
		return FALSE;
	}
	
}
